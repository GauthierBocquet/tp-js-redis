import Order from '../order/order.interface'
import OrderBuilder from '../order/order.builder'
import OrderModel from '../order/order.model'


export default class OrderService {

  public orderBuilder = new OrderBuilder()
  public orderModel = new OrderModel();

  async postOrder(data: Order): Promise<Order> {
      const rawOrders: string = await this.orderModel.getOrders()
      const orders: Order[] = JSON.parse(rawOrders) || []
      let lastId = 0
  
      if ( orders.length > 0 ) {
        const sortedOrders = orders.sort((previous: any, current: any) => {
          return current.id - previous.id
        })
        lastId = sortedOrders[0].id
      }
  
      data = {
        id: lastId + 1,
        createdAt: new Date(),
        ...data,
      }
      orders.push(data)
      await this.orderModel.setOrder(orders)
      return this.orderBuilder.makeAnonymContact(data)
  }

  async getAll(): Promise<Order[]> {
    const rawOrders: string = await this.orderModel.getOrders()
    var orders: Order[] = JSON.parse(rawOrders) || []
    return this.orderBuilder.makeAnonymContactList(orders)
  }

  async getByID(dataID: string): Promise<any> {
    const rawOrders: string = await this.orderModel.getOrders()
    const orders: Order[] = JSON.parse(rawOrders) || []

    for(var i in orders) {
      if(orders[i].id === parseInt(dataID)) {
        return this.orderBuilder.makeAnonymContact(orders[i])
      }
    }
    return []
  }

  async deleteByID(dataID: string): Promise<boolean> {
  
    const rawOrders: string = await this.orderModel.getOrders()
    const orders: Order[] = JSON.parse(rawOrders) || []

    for(var i in orders) {
      if(orders[i].id === parseInt(dataID)) {
        orders.splice(parseInt(i), 1)
        await this.orderModel.delOrder()
        await this.orderModel.setOrder(orders)
        return true;
      }
    }
    return false;
  }

  async updateByID(dataID: string, updateInformations: any): Promise<boolean> {
    const rawOrders: string = await this.orderModel.getOrders()
    const orders: Order[] = JSON.parse(rawOrders) || []
    var orderToUpdate;

    for(var i in orders) {
      if(orders[i].id === parseInt(dataID)) {
        orderToUpdate = orders[i]
      }
    }

    if(orderToUpdate === null) {
      return false;
    }

    const newOrders = {
      ...orderToUpdate,
      ...updateInformations
    }
    
    for(var i in orders) {
      if(orders[i].id === parseInt(dataID)) {
        orders.splice(parseInt(i), 1)
        orders.push(newOrders)
        await this.orderModel.delOrder()
        await this.orderModel.setOrder(orders)
        return true;
      }
    }
    return false;
  }
}