export default interface Order {
    id?: number;
    createdAt?: Date;
    packages: Package[];
    contact: Contact;
    carrier: Carrier

    addAnonymContact(): void;
}


interface Package {
    length: Taille,
    width: Taille,
    height: Taille,
    weight: Taille,
    products: Product[]
}

interface Taille {
    unit: string,
    value: number
}

interface Product {
    quantity: number,
    label: string,
    ean: string
}

interface Contact {
    firstname: string,
    lastname: string,
    phone: string,
    mail: string,
    headOfficeAddress: OfficeAddress,
    billingAddress: BillAddress,
    deliveryAddress: DeliveryAdress
}

interface BillAddress {
    postalCode: string,
      city: string,
      addressLine1: string,
      addressLine2: string,
}

interface DeliveryAdress {
    postalCode: string,
    city: string,
    addressLine1: string,
    addressLine2: string,
}

interface Carrier {
    name: string,
    contact: Contact
}

interface OfficeAddress {
    postalCode: string,
    city: string,
    addressLine1: string,
    addressLine2: string
}