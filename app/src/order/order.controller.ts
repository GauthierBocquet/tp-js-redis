import {
  Request,
  Response,
  Router,
} from 'express'

import Order from '../order/order.interface'
import OrderService from '../order/order.service'


export default class OrdersController {
  public path = '/orders'
  public pathID = '/orders/:id'
  public router = Router()
  public orderService = new OrderService();

  constructor() {
    this.intializeRoutes()
  }

  public intializeRoutes() {
    this.router.post(this.path, this.create)
    this.router.get(this.path, this.getAll)
    this.router.get(this.pathID, this.getByID)
    this.router.delete(this.pathID, this.deleteByID)
    this.router.put(this.pathID, this.updateByID)
  }

  public create = async (request: Request, response: Response ) => {
    
    if(!request.body) {
      response.status(200).json("Body is empty")
    } else {
      var data: Order = await this.orderService.postOrder(request.body)
      response.status(201).json(data)
    }
  }

  public getAll = async (request: Request, response: Response) => {
    var datas: Order[] = await this.orderService.getAll();
    response.status(200).json(datas)
  }

  public getByID = async (request: Request, response: Response) => {
    var datas: Order = await this.orderService.getByID(request.params.id);
    response.status(200).json(datas)
  }

  public deleteByID = async (request: Request, response: Response) => {
    var isDelete: boolean = await this.orderService.deleteByID(request.params.id);
    if(isDelete) {
      response.status(204).json(request.params.id + " deleted successfully")
    } else {
      return response.status(200).json("Bad ID")
    }
  }

  public updateByID = async (request: Request, response: Response) => {
    var hasBeenUpdate: boolean = await this.orderService.updateByID(request.params.id, request.body);

    if(hasBeenUpdate) {
      response.status(200).json(request.params.id + " updated successfully")
    } else {
      return response.status(200).json("Bad ID")
    }
  }
  
}
