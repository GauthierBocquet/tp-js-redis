import Order from './order.interface'

import { 
    delAsync,
    getAsync,
    setAsync } from '../../utils/storage'

export default class OrderBuilder {

    constructor() {
    }

    async setOrder(orders: Order[]) {
        await setAsync('orders', JSON.stringify(orders))
    }

    async getOrders(): Promise<string> {
        const rawOrders: string = await getAsync("orders")
        return rawOrders
    }

    async delOrder() {
        await delAsync('orders')
    }
}