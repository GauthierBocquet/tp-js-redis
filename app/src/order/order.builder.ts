import Order from './order.interface'

export default class OrderBuilder {

    constructor() {
    }

    makeAnonymContactList(orders: Order[]): Order[] {

        for (var i in orders) {
            orders[i] = this.makeAnonymContact(orders[i])
        }
        return orders;
    }

    makeAnonymContact(order: Order): Order {
    
        order.contact.firstname = "***"
        order.contact.lastname = "***"
        order.contact.phone = "***"
        order.contact.mail = "***"

        order.contact.billingAddress.postalCode = "***"
        order.contact.billingAddress.city = "***"
        order.contact.billingAddress.addressLine1 = "***"
        order.contact.billingAddress.addressLine2 = "***"

        order.contact.deliveryAddress.postalCode = "***"
        order.contact.deliveryAddress.city = "***"
        order.contact.deliveryAddress.addressLine1 = "***"
        order.contact.deliveryAddress.addressLine2 = "***"

        return order;
    }
}