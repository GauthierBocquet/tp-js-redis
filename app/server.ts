import App from './app'
import OrderController from './src/order/order.controller'


const app = new App([
  new OrderController(),
], 1337)

app.listen()
